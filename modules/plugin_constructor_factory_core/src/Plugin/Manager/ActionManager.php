<?php

namespace Drupal\plugin_constructor_factory_core\Plugin\Manager;

use Drupal\Core\Action\ActionManager as ActionManagerBase;
use Drupal\plugin_constructor_factory\ConstructorFactoryPluginManagerTrait;

/**
 * Provides an Action plugin manager.
 *
 * @see \Drupal\Core\Annotation\Action
 * @see \Drupal\Core\Action\ActionInterface
 * @see \Drupal\Core\Action\ActionBase
 * @see plugin_api
 */
class ActionManager extends ActionManagerBase {

  use ConstructorFactoryPluginManagerTrait;

}
