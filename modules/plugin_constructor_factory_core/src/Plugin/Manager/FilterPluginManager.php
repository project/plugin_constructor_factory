<?php

namespace Drupal\plugin_constructor_factory_core\Plugin\Manager;

use Drupal\filter\FilterPluginManager as FilterPluginManagerBase;
use Drupal\plugin_constructor_factory\ConstructorFactoryPluginManagerTrait;

/**
 * Manages text processing filters.
 *
 * @see hook_filter_info_alter()
 * @see \Drupal\filter\Annotation\Filter
 * @see \Drupal\filter\Plugin\FilterInterface
 * @see \Drupal\filter\Plugin\FilterBase
 * @see plugin_api
 */
class FilterPluginManager extends FilterPluginManagerBase {

  use ConstructorFactoryPluginManagerTrait;

}
