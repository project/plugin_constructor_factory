<?php

namespace Drupal\plugin_constructor_factory_core\Plugin\Manager;

use Drupal\Core\Queue\QueueWorkerManager as QueueWorkerManagerBase;
use Drupal\plugin_constructor_factory\ConstructorFactoryPluginManagerTrait;

/**
 * Overrides the core queue worker manager.
 */
class QueueWorkerManager extends QueueWorkerManagerBase {

  use ConstructorFactoryPluginManagerTrait;

}
