<?php

namespace Drupal\plugin_constructor_factory_core\Plugin;

use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;

/**
 * Base class for plugins.
 *
 * @ingroup plugin_api
 */
abstract class PluginBase {

  use StringTranslationTrait;
  use DependencySerializationTrait;
  use MessengerTrait;

}
