<?php

namespace Drupal\plugin_constructor_factory_core\Plugin;

use Drupal\Core\Queue\QueueWorkerInterface;
use Drupal\plugin_constructor_factory\Plugin\PluginInspectionInterface;
use Drupal\plugin_constructor_factory\Plugin\PluginInspectionTrait;

/**
 * Provides a base implementation for a QueueWorker plugin.
 *
 * @see \Drupal\Core\Queue\QueueWorkerInterface
 * @see \Drupal\Core\Queue\QueueWorkerManager
 * @see \Drupal\Core\Annotation\QueueWorker
 * @see plugin_api
 */
abstract class QueueWorkerBase extends PluginBase implements PluginInspectionInterface, QueueWorkerInterface {

  use PluginInspectionTrait;

}
