<?php

namespace Drupal\plugin_constructor_factory_core\Plugin;

use Drupal\Core\Action\ActionInterface;
use Drupal\plugin_constructor_factory\Plugin\PluginInspectionInterface;
use Drupal\plugin_constructor_factory\Plugin\PluginInspectionTrait;

/**
 * Provides a base implementation for an Action plugin.
 *
 * @see \Drupal\Core\Annotation\Action
 * @see \Drupal\Core\Action\ActionManager
 * @see \Drupal\Core\Action\ActionInterface
 * @see plugin_api
 */
abstract class ActionBase extends PluginBase implements PluginInspectionInterface, ActionInterface {

  use PluginInspectionTrait;

  /**
   * {@inheritdoc}
   */
  public function executeMultiple(array $entities) {
    foreach ($entities as $entity) {
      $this->execute($entity);
    }
  }

}
