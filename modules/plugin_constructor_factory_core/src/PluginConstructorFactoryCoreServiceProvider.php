<?php

namespace Drupal\plugin_constructor_factory_core;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceModifierInterface;
use Drupal\plugin_constructor_factory_core\Plugin\Manager\ActionManager;
use Drupal\plugin_constructor_factory_core\Plugin\Manager\FilterPluginManager;
use Drupal\plugin_constructor_factory_core\Plugin\Manager\QueueWorkerManager;

/**
 * Overrides core plugin managers.
 */
class PluginConstructorFactoryCoreServiceProvider implements ServiceModifierInterface {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    if ($container->hasDefinition('plugin.manager.action')) {
      $container->getDefinition('plugin.manager.action')
        ->setClass(ActionManager::class);
    }
    if ($container->hasDefinition('plugin.manager.filter')) {
      $container->getDefinition('plugin.manager.filter')
        ->setClass(FilterPluginManager::class);
    }
    if ($container->hasDefinition('plugin.manager.queue_worker')) {
      $container->getDefinition('plugin.manager.queue_worker')
        ->setClass(QueueWorkerManager::class);
    }
  }

}
