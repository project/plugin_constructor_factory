<?php

namespace Drupal\plugin_constructor_factory;

use Drupal\plugin_constructor_factory\Plugin\Factory\ConstructorFactory;

/**
 * A trait to include in plugin managers.
 *
 * @see PluginManagerBase
 */
trait ConstructorFactoryPluginManagerTrait {

  /**
   * Creates a constructor factory.
   */
  protected function getFactory() {
    if (!$this->factory) {
      $this->factory = new ConstructorFactory($this, $this->pluginInterface);
    }

    return $this->factory;
  }

}
