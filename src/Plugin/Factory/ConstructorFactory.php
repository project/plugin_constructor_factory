<?php

namespace Drupal\plugin_constructor_factory\Plugin\Factory;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Core\Plugin\Factory\ContainerFactory;
use Drupal\plugin_constructor_factory\Plugin\PluginInspectionInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Plugin factory which gets plugins from the container - if possible.
 *
 * Configuration, plugin ID & plugin definition is set through setters.
 */
class ConstructorFactory extends ContainerFactory {

  /**
   * {@inheritdoc}
   */
  public function createInstance($pluginId, array $configuration = []) {
    $pluginDefinition = $this->discovery->getDefinition($pluginId);
    $pluginClass = static::getPluginClass($pluginId, $pluginDefinition, $this->interface);
    $instance = NULL;

    // If the plugin has its service ID as annotation parameter,
    // get it from the container.
    if (!empty($pluginDefinition['service_id'])) {
      $instance = \Drupal::getContainer()->get($pluginDefinition['service_id']);
    }

    // Try to create an instance using ClassResolver.
    try {
      $instance = \Drupal::classResolver()->getInstanceFromDefinition($pluginClass);
    }
    catch (\InvalidArgumentException $e) {
    }
    catch (\ArgumentCountError $e) {
    }

    if ($instance instanceof PluginInspectionInterface) {
      try {
        $instance->setPluginId($pluginId);
        $instance->setPluginDefinition($pluginDefinition);
      }
      catch (\LogicException $e) {
        // Plugin inspection information might already be set
        // if the instance comes from the container.
      }
    }

    if ($instance instanceof ConfigurableInterface) {
      $instance->setConfiguration($configuration);
    }

    if ($instance !== NULL) {
      return $instance;
    }

    // If the plugin provides a factory method, pass the container to it.
    if (is_subclass_of($pluginClass, ContainerFactoryPluginInterface::class)) {
      return $pluginClass::create(\Drupal::getContainer(), $configuration, $pluginId, $pluginDefinition);
    }

    // Otherwise, create the plugin directly.
    return new $pluginClass($configuration, $pluginId, $pluginDefinition);

  }

}
