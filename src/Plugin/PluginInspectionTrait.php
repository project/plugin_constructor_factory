<?php

namespace Drupal\plugin_constructor_factory\Plugin;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\PluginBase;

/**
 * Defines an interface for setting extra plugin arguments using setters.
 *
 * @see DerivativeInspectionInterface
 * @see PluginInspectionInterface
 */
trait PluginInspectionTrait {

  /**
   * The plugin_id.
   *
   * @var string
   */
  protected $pluginId;

  /**
   * The plugin implementation definition.
   *
   * @var array
   */
  protected $pluginDefinition;

  /**
   * Gets the plugin_id of the plugin instance.
   *
   * @return string
   *   The plugin_id of the plugin instance.
   */
  public function getPluginId(): string {
    return $this->pluginId;
  }

  /**
   * Sets the plugin_id of the plugin instance.
   *
   * @param string $value
   *   The plugin_id of the plugin instance.
   */
  public function setPluginId(string $value): void {
    if (isset($this->pluginId)) {
      throw new \LogicException('Once set, plugin inspection information cannot be changed.');
    }

    $this->pluginId = $value;
  }

  /**
   * Gets the base_plugin_id of the plugin instance.
   *
   * @return string
   *   The base_plugin_id of the plugin instance.
   */
  public function getBaseId(): string {
    $plugin_id = $this->getPluginId();
    if (strpos($plugin_id, PluginBase::DERIVATIVE_SEPARATOR)) {
      [$plugin_id] = explode(PluginBase::DERIVATIVE_SEPARATOR, $plugin_id, 2);
    }

    return $plugin_id;
  }

  /**
   * Gets the derivative_id of the plugin instance.
   *
   * @return string|null
   *   The derivative_id of the plugin instance NULL otherwise.
   */
  public function getDerivativeId(): string {
    $pluginId = $this->getPluginId();
    $derivativeId = NULL;
    if (strpos($pluginId, PluginBase::DERIVATIVE_SEPARATOR)) {
      [, $derivativeId] = explode(PluginBase::DERIVATIVE_SEPARATOR, $pluginId, 2);
    }

    return $derivativeId;
  }

  /**
   * Gets the definition of the plugin implementation.
   *
   * @return array
   *   The plugin definition, as returned by the discovery object used by the
   *   plugin manager.
   */
  public function getPluginDefinition(): array {
    return $this->pluginDefinition;
  }

  /**
   * Sets the definition of the plugin implementation.
   *
   * @param array $value
   *   The plugin definition, as returned by the discovery object used by the
   *   plugin manager.
   */
  public function setPluginDefinition(array $value): void {
    if (isset($this->pluginDefinition)) {
      throw new \LogicException('Once set, plugin inspection information cannot be changed.');
    }

    $this->pluginDefinition = $value;
  }

  /**
   * Determines if the plugin is configurable.
   *
   * @return bool
   *   A boolean indicating whether the plugin is configurable.
   */
  public function isConfigurable(): bool {
    return $this instanceof ConfigurableInterface;
  }

}
