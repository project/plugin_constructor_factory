<?php

namespace Drupal\plugin_constructor_factory\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface as BasePluginInspectionInterface;

/**
 * Defines an interface for setting extra plugin arguments using setters.
 */
interface PluginInspectionInterface extends BasePluginInspectionInterface {

  /**
   * Sets the plugin_id of the plugin instance.
   *
   * @param string $value
   *   The plugin_id of the plugin instance.
   */
  public function setPluginId(string $value): void;

  /**
   * Sets the definition of the plugin implementation.
   *
   * @param array $value
   *   The plugin definition, as returned by the discovery object used by the
   *   plugin manager.
   */
  public function setPluginDefinition(array $value): void;

}
